package testingPackage;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;

class CalendarJUnitTest {

	@Test
	void testGetDaysDiffernce() {
		long expectedDiff = 1;
		Calendar cal = Calendar.getInstance();
		cal.set(2020, 9, 16, 0, 0, 0);
		Date inputDate = cal.getTime();
		long resultDiff = library.entities.Calendar.getInstance().getDaysDifference(inputDate);
		assertEquals(expectedDiff,resultDiff);
	}

}
